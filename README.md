cd server

virtualenv --python=python3 venv

On windows:
.\venv\Scripts\activate
On linux:
source venv/bin/activate

pip install -r requirements.txt

python manage.py makemigrations

python manage.py migrate

python manage.py runserver


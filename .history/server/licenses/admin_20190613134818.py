from django.contrib import admin
from .models import License


class BadgeAdmin(admin.ModelAdmin):
    list_display = ('id', 'device', 'os', 'browser', 'version', 'timestamp')
    search_fields = ('device',)

admin.site.register(Badge, BadgeAdmin)

from django.db import models


class License(models.Model):
    id = models.AutoField(primary_key=True)
    valid = models.BooleanField(default=True)
    create_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = ("license")
        verbose_name_plural = ("licenses")

    def __str__(self):
        return self.create_time

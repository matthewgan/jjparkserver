from django.urls import path
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

# from bund18.views import Bund18RecordViewSet, RecordSortByDate
from jjpark.views import RecordViewSet
from badge.views import BadgeViewSet, BadgeSortByDeviceView
# from device.views import CheckPinStatus, PullUpPin, PullDownPin, GetWelcomeWxConfigParameters, GetShareWxConfigParameters
# from device.views import GetWelcomeWxConfigParameters, GetShareWxConfigParameters
from device.views import GetWxConfigParametersOnStart, GetWxConfigParametersOnDisplay, GetWxConfigParametersOnShare, GetWxConfigParametersOnEnd
from share.views import ShareInfoViewSet, ShareSortByOperationView
from device.views import DeviceControlOnInterfaceView, DeviceControlOffInterfaceView, DeviceControlCheckInterfaceView

from licenses.views import GetLicenseView, UpdateLicenseView
from choices.views import PostSelectedChoiceView, ChoiceSummaryView

from jjpark.views import RecordSortByDateAll, RecordSortByDateOnlySuccess
from jjpark.views import RecordSortByWeekAll
from jjpark.views import RecordSortByHourAll
from jjpark.views import RecordSortByMonthAll


router = DefaultRouter()
# router.register(r'bund18/record', Bund18RecordViewSet, base_name='bund18')
# router.register(r'bund18/badge', BadgeViewSet, base_name='badge')
# router.register(r'bund18/share', ShareInfoViewSet, base_name='share')
router.register(r'jjpark/badge', BadgeViewSet, base_name='badge')
router.register(r'jjpark/share', ShareInfoViewSet, base_name='share')
router.register(r'jjpark/record', RecordViewSet, base_name='record')

urlpatterns = [
    # url(r'^bund18/stat', RecordSortByDate.as_view()),
    # url(r'^bund18/device/check', CheckPinStatus.as_view()),
    # url(r'^bund18/device/on', PullUpPin.as_view()),
    # url(r'^bund18/device/off', PullDownPin.as_view()),
    # url(r'^bund18/wxlogin/welcome', GetWelcomeWxConfigParameters.as_view()),
    # url(r'^bund18/wxlogin/share', GetShareWxConfigParameters.as_view()),

    # url(r'^jjpark/wxlogin/welcome', GetWelcomeWxConfigParameters.as_view()),
    url(r'^jjpark/wxlogin/start', GetWxConfigParametersOnStart.as_view()),
    url(r'^jjpark/wxlogin/display', GetWxConfigParametersOnDisplay.as_view()),
    url(r'^jjpark/wxlogin/share', GetWxConfigParametersOnShare.as_view()),
    url(r'^jjpark/wxlogin/end', GetWxConfigParametersOnEnd.as_view()),
    # url(r'^jjpark/wxlogin', GetWxConfigParameters.as_view()),

    url(r'jjpark/device/control/request', GetLicenseView.as_view()),
    url(r'jjpark/device/control/release/(?P<pk>[0-9]+)', UpdateLicenseView.as_view()),
    url(r'jjpark/device/control/on/(?P<pk>[1-6])', DeviceControlOnInterfaceView.as_view()),
    url(r'jjpark/device/control/off/(?P<pk>[1-6])', DeviceControlOffInterfaceView.as_view()),
    url(r'jjpark/device/control/(?P<pk>[1-6])', DeviceControlCheckInterfaceView.as_view()),

    url(r'jjpark/choice/report', PostSelectedChoiceView.as_view()),

    # Record Filters
    url(r'^stat/month', RecordSortByMonthAll.as_view()),
    url(r'^stat/week', RecordSortByWeekAll.as_view()),
    url(r'^stat/day', RecordSortByDateAll.as_view()),
    url(r'^stat/hour', RecordSortByHourAll.as_view()),

    url(r'^stat/device', BadgeSortByDeviceView.as_view()),
    url(r'^stat/operation', ShareSortByOperationView.as_view()),
    url(r'^stat/choice', ChoiceSummaryView.as_view()),
]

urlpatterns += router.urls

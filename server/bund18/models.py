from django.db import models

class VisitRecord(models.Model):    
    badgeID = models.IntegerField(default=0)
    device = models.CharField(max_length=20, default='Bund18')
    operation = models.CharField(max_length=20, default='shake')
    status = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.timestamp)
 

class RecordSortByDate(models.Model):
    date_created = models.DateField()
    created_count = models.IntegerField(default=0)

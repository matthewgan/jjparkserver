from django.db.models.functions import TruncDate
from django.db.models import Count
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
import json
from django.db.models import Q

from .models import VisitRecord
from .serializer import VisitRecordSerializer, VisitRecordGroupByDateSerializer


class Bund18RecordViewSet(ModelViewSet):
    queryset = VisitRecord.objects.all()
    serializer_class = VisitRecordSerializer


class RecordSortByDate(APIView):
    def get(self, request, format=None):
        result = VisitRecord.objects.filter(Q(operation="shake") & Q(status=True) ).extra({'date_created': "date(timestamp)"}).values('date_created').annotate(created_count=Count('badgeID'))
        serializer = VisitRecordGroupByDateSerializer(result, many=True)
        return Response(serializer.data)


from django.db import models


class Badge(models.Model):
    browser = models.CharField(max_length=50)
    version = models.CharField(max_length=50)
    device = models.CharField(max_length=50)
    os = models.CharField(max_length=20)    
    timestamp = models.DateTimeField(auto_now_add=True)    

    class Meta:
        verbose_name = ("device")
        verbose_name_plural = ("devices")

    def __str__(self):
        return str(self.timestamp)


class BadgeSortByDate(models.Model):
    device = models.CharField(max_length=50)
    created_count = models.IntegerField(default=0)


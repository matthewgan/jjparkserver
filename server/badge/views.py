from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Count
from .models import Badge
from .serializers import BadgeSerializer, BadgeSortByDeviceSerializer


class BadgeViewSet(ModelViewSet):
    queryset = Badge.objects.all()
    serializer_class = BadgeSerializer


class BadgeSortByDeviceView(APIView):
    def get(self, request):
        result = Badge.objects.all().extra({'device': "os"})\
            .values('device').annotate(created_count=Count('id'))
        serializer = BadgeSortByDeviceSerializer(result, many=True)
        return Response(serializer.data)


from rest_framework import serializers
from .models import Badge, BadgeSortByDate


class BadgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Badge
        fields = '__all__'
        read_only_fields = ('timestamp', )


class BadgeSortByDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = BadgeSortByDate
        fields = ['device', 'created_count']

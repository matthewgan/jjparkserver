from django.contrib import admin
from .models import License


class LicenseAdmin(admin.ModelAdmin):
    list_display = ('id', 'valid', 'create_time')
    search_fields = ('create_time',)


admin.site.register(License, LicenseAdmin)

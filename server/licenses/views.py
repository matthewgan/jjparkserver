import pytz
from datetime import datetime, timedelta
from django.utils import timezone
from rest_framework.views import APIView
# from rest_framework.generics import UpdateAPIView
from rest_framework.response import Response
from rest_framework import status

from .models import License
from .serializers import LicenseSerializer


class GetLicenseView(APIView):
    def get(self, request):
        try:
            last_license = License.objects.latest('create_time')
            # check last license is valid or out of time
            if last_license.valid:
                c_time = last_license.create_time
                over_time = c_time + timedelta(seconds=40)
                # print(over_time)
                # print(timezone.now())
                # check the time now
                if timezone.now() >= over_time:
                    last_license.valid = False
                    last_license.save()
                    new_license = License.objects.create()
                    serializer = LicenseSerializer(new_license)
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response("Busy", status=status.HTTP_401_UNAUTHORIZED)
            else:
                new_license = License.objects.create()
                serializer = LicenseSerializer(new_license)
                return Response(serializer.data, status=status.HTTP_201_CREATED)

        except License.DoesNotExist:
            # First Visitor
            new_license = License.objects.create()
            serializer = LicenseSerializer(new_license)
            return Response(serializer.data, status=status.HTTP_201_CREATED)


class UpdateLicenseView(APIView):
    def get(self, request, pk):
        try:
            current_license = License.objects.get(pk=pk)
            # print(current_license)
            current_license.valid = False
            current_license.save()
            serializer = LicenseSerializer(current_license)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except License.DoesNotExist:
            return Response("License not found", status=status.HTTP_404_NOT_FOUND)

from rest_framework import serializers
from .models import ShareInfo, ShareSortByOperation


class ShareInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShareInfo
        fields = '__all__'
        read_only_fields = ('timestamp', )


class ShareSortByOperationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShareSortByOperation
        fields = ['operation', 'created_count']

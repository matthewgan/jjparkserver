from django.contrib import admin
from .models import ShareInfo


class ShareInfoAdmin(admin.ModelAdmin):
    list_display = ('id','badgeID', 'selectID','operation', 'timestamp')
    search_fields = ('randomID', 'operation', )


admin.site.register(ShareInfo, ShareInfoAdmin)

from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from django.db.models import Count
from .models import ShareInfo
from .serializers import ShareInfoSerializer, ShareSortByOperationSerializer


class ShareInfoViewSet(ModelViewSet):
    queryset = ShareInfo.objects.all()
    serializer_class = ShareInfoSerializer
    

class ShareSortByOperationView(APIView):
    def get(self, request):
        result = ShareInfo.objects.all().extra({'operation': "operation"}) \
            .values('operation').annotate(created_count=Count('id'))
        serializer = ShareSortByOperationSerializer(result, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

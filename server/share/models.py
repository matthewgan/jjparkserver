from django.db import models


class ShareInfo(models.Model):
    badgeID = models.IntegerField()
    selectID = models.IntegerField()
    operation = models.CharField(max_length=50, default="moments")
    timestamp = models.DateTimeField(auto_now_add=True)


class ShareSortByOperation(models.Model):
    operation = models.CharField(max_length=50)
    created_count = models.IntegerField()

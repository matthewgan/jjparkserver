from django.contrib import admin
from .models import VisitRecord


class VisitRecordAdmin(admin.ModelAdmin):
    list_display = ('id', 'badgeID', 'operation', 'device', 'status', 'timestamp')
    search_fields = ('badge',)


admin.site.register(VisitRecord, VisitRecordAdmin)

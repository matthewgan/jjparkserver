from django.db.models.functions import TruncDate
from django.db.models import Count
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import json
from django.db.models import Q

from .models import VisitRecord
from .serializers import VisitRecordSerializer, VisitRecordGroupByDateSerializer


class RecordViewSet(ModelViewSet):
    queryset = VisitRecord.objects.all()
    serializer_class = VisitRecordSerializer


class RecordSortByDateOnlySuccess(APIView):
    def get(self, request):
        result = VisitRecord.objects.filter(Q(operation="select") & Q(status=True))\
            .extra({'date_created': "date(timestamp)"})\
            .values('date_created')\
            .annotate(created_count=Count('badgeID'))
        serializer = VisitRecordGroupByDateSerializer(result, many=True)
        return Response(serializer.data)


class RecordSortByDateAll(APIView):
    def get(self, request):
        result = VisitRecord.objects.filter(operation="select")\
            .extra({'date_created': "date(timestamp)"})\
            .values('date_created')\
            .annotate(created_count=Count('badgeID'))
        serializer = VisitRecordGroupByDateSerializer(result, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class RecordSortByWeekAll(APIView):
    def get(self, request):
        result = VisitRecord.objects.filter(operation="select")
        ret_dict_list = []
        dict = {}

        for w in range(1, 54):
            result2 = result.filter(timestamp__week=w)
            count = len(result2)
            if count > 0:
                dict = {'week': w, 'count': count}
                ret_dict_list.append(dict)
            print(dict)
        # serializer = VisitRecordGroupByDateSerializer(result, many=True)
        return Response(json.dumps(ret_dict_list), status=status.HTTP_200_OK)


class RecordSortByHourAll(APIView):
    def get(self, request):
        result = VisitRecord.objects.filter(operation="select")
        ret_dict_list = []
        dict = {}

        for h in range(0, 25):
            result2 = result.filter(timestamp__hour=h)
            count = len(result2)
            if count > 0:
                dict = {'hour': h, 'count': count}
                ret_dict_list.append(dict)
            print(dict)
        # serializer = VisitRecordGroupByDateSerializer(result, many=True)
        return Response(json.dumps(ret_dict_list), status=status.HTTP_200_OK)


class RecordSortByMonthAll(APIView):
    def get(self, request):
        result = VisitRecord.objects.filter(operation="select")
        ret_dict_list = []
        dict = {}

        for h in range(1, 13):
            result2 = result.filter(timestamp__month=h)
            count = len(result2)
            if count > 0:
                dict = {'month': h, 'count': count}
                ret_dict_list.append(dict)
            print(dict)
        # serializer = VisitRecordGroupByDateSerializer(result, many=True)
        return Response(json.dumps(ret_dict_list), status=status.HTTP_200_OK)

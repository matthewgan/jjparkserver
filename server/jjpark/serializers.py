from rest_framework import serializers

from .models import VisitRecord, RecordSortByDate


class VisitRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisitRecord
        fields = '__all__'
        read_only_fields = ('timestamp', )


class VisitRecordGroupByDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecordSortByDate
        fields = ('date_created', 'created_count')

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import requests
import json
import uuid
import datetime
import time
from hashlib import sha1

from .models import AccessToken, JsApiTicket
from datetime import datetime, timedelta
from django.utils import timezone


def createNonceStr():
    return str(uuid.uuid4())


def createTimestamp():
    print((datetime.datetime.now()))
    return str(datetime.datetime.now())


def getAppId():
    return "wx34cbcfd9b9889a92"


def getAppSecret():
    return "02f0e24aff6e944a5849d79061568b06"


def getAccessToken():
    APPID = getAppId()
    APPSECRET = getAppSecret()
    url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + APPID + "&secret=" + APPSECRET
    return requests.request("GET", url)


def getJsApiTicket(ACCESS_TOKEN):
    url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + ACCESS_TOKEN + "&type=jsapi"
    return requests.request("GET", url)


def mapToPinNumber(id):
    return id + 5


def getControlDomain():
    return "http://54.223.102.79:81/arduino/digital/"


def check_time_not_expired(create_time):
    over_time = create_time + timedelta(seconds=7200)
    if timezone.now() >= over_time:
        # Not valid any more, need to refresh
        return False
    else:
        # Still Valid
        return True


def generate_wx_parameters(jsapi_ticket, page_url):
    noncestr = createNonceStr()
    timestamp = int(time.time())
    string1 = 'jsapi_ticket=' + jsapi_ticket \
              + '&noncestr=' + noncestr \
              + '&timestamp=' + str(timestamp) \
              + '&url=' + page_url
    print(string1)
    signature = sha1(string1.encode('utf-8')).hexdigest()
    print(signature)
    wxconfig_paras = {
        'appId': getAppId(),
        'timestamp': timestamp,
        'nonceStr': noncestr,
        'signature': signature,
    }
    return wxconfig_paras


class CheckPinStatus(APIView):
    def get(self, request):
        url = "http://tunnel40057.remotuino.wuzhanggui.shop:81/arduino/digital/13/"
        response = requests.request("GET", url)
        print(response)
        print(response.content)
        if response.status_code == 200:
            return Response(status.HTTP_200_OK)
        else:
            return Response(status.HTTP_404_NOT_FOUND)
        

class PullUpPin(APIView):
    def get(self, request):
        url = "http://tunnel40057.remotuino.wuzhanggui.shop:81/arduino/digital/13/1/"
        response = requests.request("GET", url)
        print(response)
        print(response.content)
        if response.status_code == 200:
            return Response(status.HTTP_200_OK)
        else:
            return Response(status.HTTP_404_NOT_FOUND)


class PullDownPin(APIView):
    def get(self, request):
        url = "http://tunnel40057.remotuino.wuzhanggui.shop:81/arduino/digital/13/0/"
        response = requests.request("GET", url)
        print(response)
        print(response.content)
        if response.status_code == 200:
            return Response(status.HTTP_200_OK)
        else:
            return Response(status.HTTP_404_NOT_FOUND)


class GetShareWxConfigParameters(APIView):
    def get(self, request):
        print(request.data)
        extra_url = "http://www.mobiwind.cn/index.php?g=User&m=AutoWechatOauth2&a=getWechatTicketCacheData&appid=wx5f60e7a12dacdeb2"
        response = requests.request("GET", extra_url)
        print(response.text)
        json_data = json.loads(response.text)
        print(type(json_data))
        jsapi_ticket = json_data.get('JsApi_Ticket')
        data = json_data.get('data')
        timestamp = data.get('expire_time')
        print(timestamp)

        page_url = "http://mm.wuzhanggui.shop/bund18/share"
        noncestr = createNonceStr()
        string1 = 'jsapi_ticket=' + jsapi_ticket + '&noncestr=' + noncestr + '&timestamp=' + str(timestamp) + '&url=' + page_url
        print(string1)
        signature = sha1(string1.encode('utf-8')).hexdigest()

        print(signature)

        wxconfig_paras = {
            'appId': "wx5f60e7a12dacdeb2",
            'timestamp': timestamp,
            'nonceStr': noncestr,
            'signature': signature,
        }
        print(wxconfig_paras)
        
        return Response(wxconfig_paras)


class GetWelcomeWxConfigParameters(APIView):
    def get(self, request):
        print(request.data)
        extra_url = "http://www.mobiwind.cn/index.php?g=User&m=AutoWechatOauth2&a=getWechatTicketCacheData&appid=wx5f60e7a12dacdeb2"
        response = requests.request("GET", extra_url)
        print(response.text)
        json_data = json.loads(response.text)
        print(type(json_data))
        jsapi_ticket = json_data.get('JsApi_Ticket')
        data = json_data.get('data')
        timestamp = data.get('expire_time')
        print(timestamp)

        page_url = "http://mm.wuzhanggui.shop/"
        noncestr = createNonceStr()
        string1 = 'jsapi_ticket=' + jsapi_ticket + '&noncestr=' + noncestr + '&timestamp=' + str(timestamp) + '&url=' + page_url
        print(string1)
        signature = sha1(string1.encode('utf-8')).hexdigest()

        print(signature)

        wxconfig_paras = {
            'appId': "wx5f60e7a12dacdeb2",
            'timestamp': timestamp,
            'nonceStr': noncestr,
            'signature': signature,
        }
        print(wxconfig_paras)
        
        return Response(wxconfig_paras)


class DeviceControlCheckInterfaceView(APIView):
    def get(self, request, pk):
        control_in = int(pk)
        pin_number = mapToPinNumber(control_in)
        url = getControlDomain() + str(pin_number) + '/'
        response = requests.request("GET", url)
        print(response)
        print(response.content)
        if response.status_code == 200:
            return Response(response.content, status=status.HTTP_200_OK)
        else:
            return Response(status.HTTP_404_NOT_FOUND)


class DeviceControlOnInterfaceView(APIView):
    def get(self, request, pk):
        control_in = int(pk)
        pin_number = mapToPinNumber(control_in)
        url = getControlDomain() + str(pin_number) + '/0/'
        response = requests.request("GET", url)
        print(response)
        print(response.content)
        if response.status_code == 200:
            return Response(response.content, status=status.HTTP_200_OK)
        else:
            return Response(status.HTTP_404_NOT_FOUND)


class DeviceControlOffInterfaceView(APIView):
    def get(self, request, pk):
        control_in = int(pk)
        pin_number = mapToPinNumber(control_in)
        url = getControlDomain() + str(pin_number) + '/1/'
        response = requests.request("GET", url)
        print(response)
        print(response.content)
        if response.status_code == 200:
            return Response(response.content, status=status.HTTP_200_OK)
        else:
            return Response(status.HTTP_404_NOT_FOUND)


class GetWxConfigParametersOnDisplay(APIView):
    def get(self, request):
        # First Check if Valid Ticket is saved in server
        page_url = "http://www.fullrank.top/display"

        # Step 1: Check access_token is valid
        try:
            last_token = AccessToken.objects.latest('timestamp')
            token_valid = check_time_not_expired(last_token.timestamp)
            if token_valid is True:
                valid_token = last_token
            else:
                # Request a new access_token
                response = getAccessToken()
                print(response.text)
                json_data = json.loads(response.text)
                access_token = json_data.get('access_token')
                expires_in = json_data.get('expires_in')
                new_token = AccessToken.objects.create(access_token=access_token, expires_in=expires_in)
                valid_token = new_token

        except AccessToken.DoesNotExist:
            # Need one new access_token
            response = getAccessToken()
            print(response.text)
            json_data = json.loads(response.text)
            access_token = json_data.get('access_token')
            expires_in = json_data.get('expires_in')
            new_token = AccessToken.objects.create(access_token=access_token, expires_in=expires_in)
            valid_token = new_token

        # Step 2: Check JsApi Ticket is valid
        try:
            last_ticket = JsApiTicket.objects.latest('timestamp')
            ticket_valid = check_time_not_expired(last_ticket.timestamp)
            if ticket_valid is True:
                paras = generate_wx_parameters(last_ticket.ticket, page_url)
            else:
                # need to refresh jsapi-ticket using valid_token
                response = getJsApiTicket(valid_token.access_token)
                print(response.text)
                json_data = json.loads(response.text)
                jsapi_ticket = json_data.get('ticket')
                expires_in = json_data.get('expires_in')
                new_ticket = JsApiTicket.objects.create(ticket=jsapi_ticket, expires_in=expires_in)
                paras = generate_wx_parameters(new_ticket.ticket, page_url)

        except JsApiTicket.DoesNotExist:
            # Need one new js ticket
            response = getJsApiTicket(valid_token.access_token)
            print(response.text)
            json_data = json.loads(response.text)
            jsapi_ticket = json_data.get('ticket')
            expires_in = json_data.get('expires_in')
            new_ticket = JsApiTicket.objects.create(ticket=jsapi_ticket, expires_in=expires_in)
            paras = generate_wx_parameters(new_ticket.ticket, page_url)

        return Response(paras, status=status.HTTP_200_OK)


class GetWxConfigParametersOnShare(APIView):
    def get(self, request):
        # First Check if Valid Ticket is saved in server
        page_url = "http://www.fullrank.top/share"

        # Step 1: Check access_token is valid
        try:
            last_token = AccessToken.objects.latest('timestamp')
            token_valid = check_time_not_expired(last_token.timestamp)
            if token_valid is True:
                valid_token = last_token
            else:
                # Request a new access_token
                response = getAccessToken()
                print(response.text)
                json_data = json.loads(response.text)
                access_token = json_data.get('access_token')
                expires_in = json_data.get('expires_in')
                new_token = AccessToken.objects.create(access_token=access_token, expires_in=expires_in)
                valid_token = new_token

        except AccessToken.DoesNotExist:
            # Need one new access_token
            response = getAccessToken()
            print(response.text)
            json_data = json.loads(response.text)
            access_token = json_data.get('access_token')
            expires_in = json_data.get('expires_in')
            new_token = AccessToken.objects.create(access_token=access_token, expires_in=expires_in)
            valid_token = new_token

        # Step 2: Check JsApi Ticket is valid
        try:
            last_ticket = JsApiTicket.objects.latest('timestamp')
            ticket_valid = check_time_not_expired(last_ticket.timestamp)
            if ticket_valid is True:
                paras = generate_wx_parameters(last_ticket.ticket, page_url)
            else:
                # need to refresh jsapi-ticket using valid_token
                response = getJsApiTicket(valid_token.access_token)
                print(response.text)
                json_data = json.loads(response.text)
                jsapi_ticket = json_data.get('ticket')
                expires_in = json_data.get('expires_in')
                new_ticket = JsApiTicket.objects.create(ticket=jsapi_ticket, expires_in=expires_in)
                paras = generate_wx_parameters(new_ticket.ticket, page_url)

        except JsApiTicket.DoesNotExist:
            # Need one new js ticket
            response = getJsApiTicket(valid_token.access_token)
            print(response.text)
            json_data = json.loads(response.text)
            jsapi_ticket = json_data.get('ticket')
            expires_in = json_data.get('expires_in')
            new_ticket = JsApiTicket.objects.create(ticket=jsapi_ticket, expires_in=expires_in)
            paras = generate_wx_parameters(new_ticket.ticket, page_url)

        return Response(paras, status=status.HTTP_200_OK)


class GetWxConfigParametersOnEnd(APIView):
    def get(self, request):
        # First Check if Valid Ticket is saved in server
        page_url = "http://www.fullrank.top/end"

        # Step 1: Check access_token is valid
        try:
            last_token = AccessToken.objects.latest('timestamp')
            token_valid = check_time_not_expired(last_token.timestamp)
            if token_valid is True:
                valid_token = last_token
            else:
                # Request a new access_token
                response = getAccessToken()
                print(response.text)
                json_data = json.loads(response.text)
                access_token = json_data.get('access_token')
                expires_in = json_data.get('expires_in')
                new_token = AccessToken.objects.create(access_token=access_token, expires_in=expires_in)
                valid_token = new_token

        except AccessToken.DoesNotExist:
            # Need one new access_token
            response = getAccessToken()
            print(response.text)
            json_data = json.loads(response.text)
            access_token = json_data.get('access_token')
            expires_in = json_data.get('expires_in')
            new_token = AccessToken.objects.create(access_token=access_token, expires_in=expires_in)
            valid_token = new_token

        # Step 2: Check JsApi Ticket is valid
        try:
            last_ticket = JsApiTicket.objects.latest('timestamp')
            ticket_valid = check_time_not_expired(last_ticket.timestamp)
            if ticket_valid is True:
                paras = generate_wx_parameters(last_ticket.ticket, page_url)
            else:
                # need to refresh jsapi-ticket using valid_token
                response = getJsApiTicket(valid_token.access_token)
                print(response.text)
                json_data = json.loads(response.text)
                jsapi_ticket = json_data.get('ticket')
                expires_in = json_data.get('expires_in')
                new_ticket = JsApiTicket.objects.create(ticket=jsapi_ticket, expires_in=expires_in)
                paras = generate_wx_parameters(new_ticket.ticket, page_url)

        except JsApiTicket.DoesNotExist:
            # Need one new js ticket
            response = getJsApiTicket(valid_token.access_token)
            print(response.text)
            json_data = json.loads(response.text)
            jsapi_ticket = json_data.get('ticket')
            expires_in = json_data.get('expires_in')
            new_ticket = JsApiTicket.objects.create(ticket=jsapi_ticket, expires_in=expires_in)
            paras = generate_wx_parameters(new_ticket.ticket, page_url)

        return Response(paras, status=status.HTTP_200_OK)


class GetWxConfigParametersOnStart(APIView):
    def get(self, request):
        # First Check if Valid Ticket is saved in server
        page_url = "http://www.fullrank.top/"

        # Step 1: Check access_token is valid
        try:
            last_token = AccessToken.objects.latest('timestamp')
            token_valid = check_time_not_expired(last_token.timestamp)
            if token_valid is True:
                valid_token = last_token
            else:
                # Request a new access_token
                response = getAccessToken()
                print(response.text)
                json_data = json.loads(response.text)
                access_token = json_data.get('access_token')
                expires_in = json_data.get('expires_in')
                new_token = AccessToken.objects.create(access_token=access_token, expires_in=expires_in)
                valid_token = new_token

        except AccessToken.DoesNotExist:
            # Need one new access_token
            response = getAccessToken()
            print(response.text)
            json_data = json.loads(response.text)
            access_token = json_data.get('access_token')
            expires_in = json_data.get('expires_in')
            new_token = AccessToken.objects.create(access_token=access_token, expires_in=expires_in)
            valid_token = new_token

        # Step 2: Check JsApi Ticket is valid
        try:
            last_ticket = JsApiTicket.objects.latest('timestamp')
            ticket_valid = check_time_not_expired(last_ticket.timestamp)
            if ticket_valid is True:
                paras = generate_wx_parameters(last_ticket.ticket, page_url)
            else:
                # need to refresh jsapi-ticket using valid_token
                response = getJsApiTicket(valid_token.access_token)
                print(response.text)
                json_data = json.loads(response.text)
                jsapi_ticket = json_data.get('ticket')
                expires_in = json_data.get('expires_in')
                new_ticket = JsApiTicket.objects.create(ticket=jsapi_ticket, expires_in=expires_in)
                paras = generate_wx_parameters(new_ticket.ticket, page_url)

        except JsApiTicket.DoesNotExist:
            # Need one new js ticket
            response = getJsApiTicket(valid_token.access_token)
            print(response.text)
            json_data = json.loads(response.text)
            jsapi_ticket = json_data.get('ticket')
            expires_in = json_data.get('expires_in')
            new_ticket = JsApiTicket.objects.create(ticket=jsapi_ticket, expires_in=expires_in)
            paras = generate_wx_parameters(new_ticket.ticket, page_url)

        return Response(paras, status=status.HTTP_200_OK)

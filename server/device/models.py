from django.db import models


class AccessToken(models.Model):
    access_token = models.CharField(max_length=512)
    expires_in = models.IntegerField(default=7200)
    timestamp = models.DateTimeField(auto_now_add=True)


class JsApiTicket(models.Model):
    ticket = models.CharField(max_length=512)
    expires_in = models.IntegerField(default=7200)
    timestamp = models.DateTimeField(auto_now_add=True)

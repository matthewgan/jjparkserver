from django.contrib import admin
from .models import AccessToken, JsApiTicket


admin.site.register(AccessToken)
admin.site.register(JsApiTicket)

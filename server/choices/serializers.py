from rest_framework import serializers
from .models import SelectChoice, ChoiceSummary


class SelectChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = SelectChoice
        fields = '__all__'
        read_only_fields = ['timestamp', ]


class PostSelectedChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = SelectChoice
        fields = ['choice']


class ChoiceSummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = ChoiceSummary
        fields = ['choice', 'created_count']

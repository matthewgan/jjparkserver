from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.db.models import Count
from .models import SelectChoice
from .serializers import SelectChoiceSerializer, PostSelectedChoiceSerializer, ChoiceSummarySerializer


class PostSelectedChoiceView(APIView):
    def post(self, request):
        sid = request.data.get('choice')
        print(sid)
        if 1 <= (int(sid)) <= 6:
            choice = SelectChoice(choice=sid)
            choice.save()
            serializer = SelectChoiceSerializer(choice)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class ChoiceSummaryView(APIView):
    def get(self, request):
        result = SelectChoice.objects.all().extra({'choice': "choice"})\
            .values('choice').annotate(created_count=Count('id'))
        serializer = ChoiceSummarySerializer(result, many=True)
        return Response(serializer.data)


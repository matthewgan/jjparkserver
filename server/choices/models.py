from django.db import models


class SelectChoice(models.Model):
    choice = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True)


class ChoiceSummary(models.Model):
    choice = models.IntegerField()
    created_count = models.IntegerField()
